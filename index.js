const express = require('express');
const socket = require('socket.io');
const Datastore = require('nedb');
const PORT = process.env.PORT || process.argv[2] || 3000;

var db = new Datastore({
    filename: './data/boards.json',
    autoload: true
});

const app = express();
const server = app.listen(PORT, function () {
    console.log('Server is running on http://localhost:' + PORT);
});

// Socket.IO
const io = socket(server);

// Routes
app.use(require('./routes')(db, io));