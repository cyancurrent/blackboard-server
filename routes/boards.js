const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const Hashids = require('hashids');

var sockets = require('../sockets');
var router = express.Router();
var jsonParser = bodyParser.json();
var hashids = new Hashids();

module.exports = function (db, io) {
    router.post('/', cors(), jsonParser, function (req, res) {
        var namespace = hashids.encode(new Date().getTime());
        var name = req.body.name;
        var private = req.body.private;
        var password = req.body.password;
        db.insert({
            name: name,
            nsp: namespace,
            private: private,
            password: password,
            content: []
        }, function (error, newDoc) {
            if (error == null) {
                console.log('Inserted new record into boards, with _id = ' + newDoc["_id"]);
                sockets.setupNamespace(io, namespace, db);
                res.status(200).send({
                    id: newDoc["_id"],
                    nsp: namespace
                });
            }
            else {
                res.status(500).send('Internal server error.');
            }
        });
    });

    router.get('/:board', cors(), function (req, res) {
        var board = req.params.board;
        var password = req.query.pass;
        db.find({ "_id": board }, function (error, docs) {
            if (error == null) {
                if (docs.length > 0) {
                    var record = docs[0];
                    if (record["private"]) {
                        if (password == null) {
                            res.status(401).send('Requested board requires password');
                        }
                        else if (password === record["password"]) {
                            res.status(200).send({
                                nsp: record["nsp"]
                            });
                        }
                        else {
                            res.status(403).send('Password is incorrect');
                        }
                    }
                    else {
                        res.status(200).send({
                            nsp: record["nsp"]
                        });
                    }
                }
                else {
                    res.status(404).send('Board not found');
                }

            }
            else {
                res.status(500).send('Internal server error.');
            }
        });
    });

    return router;
};