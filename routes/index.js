const express = require('express');
const cors = require('cors');

var router = express.Router();

module.exports = function(db, io) {
    router.use('/boards', cors(), require('./boards')(db, io));
    router.use('/state', cors(), require('./state')(db, io));
    return router;
};