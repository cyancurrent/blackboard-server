const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');

var router = express.Router();
var jsonParser = bodyParser.json();

module.exports = function (db, io) {
    router.get('/', cors(), function (req, res) {
        res.status(200).send('ALIVE');
    });

    return router;
};