module.exports = {
    setupNamespace: function (io, namespace, db) {
        var nsp = io.of('/' + namespace);
        nsp.on('connection', function (socket) {
            console.log((new Date().toLocaleString()) + ': Socket ' + socket.id + ' connected to ' + namespace);
            // Notify others about new client
            socket.broadcast.emit('join', { socketID: socket.id });
            // Pushing current state of the board to newly connected client
            nsp.clients(function (error, clients) {
                if (error) throw error;
                console.log(clients); // => [PZDoMHjiu8PYfRiKAAAF, Anw2LatarvGVVXEIAAAD]
                db.findOne({ nsp: namespace }, function (err, doc) {
                    if (err) throw err;
                    nsp.to(socket.id).emit('fetch', {
                        clients: clients,
                        content: doc["content"]
                    });
                });
            });
            // On client update
            socket.on('update', function (payload) {
                console.log('Received update request from ' + socket.id);
                db.update({ nsp: namespace }, { $push: { content: payload } }, function (error, numReplaced) {
                    // Push updates
                });
                socket.broadcast.emit('update', payload);
            });
            // Before client disconnected, notify others about its leave
            socket.on('disconnecting', function () {
                console.log((new Date().toLocaleString()) + ': Socket ' + socket.id + ' about to diconnect from ' + namespace);
                socket.broadcast.emit('leave', { socketID: socket.id });
            });
        });
    }
};
