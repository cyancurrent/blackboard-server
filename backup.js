const express = require('express');
const socket = require('socket.io');
const axios = require('axios');
const Datastore = require('nedb');
const PORT = process.env.PORT || process.argv[2] || 3001;

var db = new Datastore({
    filename: './data/boards.json'
});

var sockets = require('./sockets');

var primaryOff = false;
const primaryHost = 'http://localhost:3000';
const ackDelay = 5000;

function restoreSessions() {
    db.find({}, function (error, docs) {
        if (error == null) {
            for (var i = 0; i < docs.length; i++) {
                var session = docs[i];
                console.log('Restoring session for ' + session["nsp"]);
                sockets.setupNamespace(io, session["nsp"], db);
            }
        }
        else {
            console.log(error);
        }
    });
};

const app = express();
const server = app.listen(PORT, function () {
    console.log('Server is running on http://localhost:' + PORT);
    var scheduler = setInterval(function () {
        axios.get(primaryHost + '/state').then(function (response) {
            console.log('Primary alive');
        }).catch(function (error) {
            console.log('Primary does not respond, switch to serving');
            clearInterval(scheduler);
            db.loadDatabase(function (err) {
                if (err == null) {
                    app.use(require('./routes')(db, io));
                    restoreSessions();
                }
                else {
                    console.log('Could not load database: ' + err);
                }
            });
        });
    }, ackDelay);
});

// Socket.IO
const io = socket(server);